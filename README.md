# React Tailwind TypeScript

This project is a React application that utilizes Vite, TailwindCSS, and TypeScript. It supports dark mode and incorporates Redux and Redux Persist for state management. The code is formatted using Prettier, and the project uses React Router DOM for routing.

## Installation

Clone this project from [GitLab](https://gitlab.com/jirapat-su/react-tailwind-typescript).

```bash
git clone https://gitlab.com/jirapat-su/react-tailwind-typescript.git
```

## Environment Variables

This project can utilize environment variables. Please refer to the [Vite documentation](https://vitejs.dev/guide/env-and-mode.html) for more information on how to configure and use environment variables.

## Recommended Visual Studio Code Configuration

For optimal development experience, it is recommended to use the following Visual Studio Code configuration:

```json
{
  "editor.defaultFormatter": "esbenp.prettier-vscode",
  "editor.formatOnSave": true,
  "editor.formatOnType": true,
  "editor.tabSize": 2,
  "editor.codeActionsOnSave": {
    "source.fixAll": true,
    "source.organizeImports": true
  },
  "typescript.referencesCodeLens.enabled": true,
  "typescript.updateImportsOnFileMove.enabled": "always",
  "prettier.printWidth": 250,
  "css.lint.unknownAtRules": "ignore"
}
```

## Recommended Extensions

The following extensions are recommended for a better development experience:

- [Prettier - Code formatter](https://marketplace.visualstudio.com/items?itemName=esbenp.prettier-vscode)
- [Path Intellisense](https://marketplace.visualstudio.com/items?itemName=christian-kohler.path-intellisense)
- [Tailwind CSS IntelliSense](https://marketplace.visualstudio.com/items?itemName=bradlc.vscode-tailwindcss)
- [Auto Close Tag](https://marketplace.visualstudio.com/items?itemName=formulahendry.auto-close-tag)
- [Auto Rename Tag](https://marketplace.visualstudio.com/items?itemName=formulahendry.auto-rename-tag)
- [npm Intellisense](https://marketplace.visualstudio.com/items?itemName=christian-kohler.npm-intellisense)

## Usage

- Development mode: `npm run dev`
- Build for production: `npm run build`
- Preview production locally: `npm run preview`
- Code formatter: `npm run format`
- Linting: `npm run lint`

## License

This project is licensed under the [MIT License.](https://en.wikipedia.org/wiki/MIT_License)
