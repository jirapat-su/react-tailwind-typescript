import { lazy } from 'react';
import { Route, createBrowserRouter, createRoutesFromElements } from 'react-router-dom';
import App from './App';

// Layout Components
const MainLayout = lazy(() => import('@/core/components/layout/mainLayout'));

// Page Components
const HomePage = lazy(() => import('@/pages/home'));
const AboutPage = lazy(() => import('@/pages/about'));

// Other Components
const PageNotFound = lazy(() => import('@/core/components/ui/pageNotFound'));

export const router = createBrowserRouter(
  createRoutesFromElements(
    <Route path="/" element={<App />}>
      <Route path="" element={<MainLayout />}>
        <Route index element={<HomePage />} />
        <Route path="about" element={<AboutPage />} />
        <Route path="*" element={<PageNotFound />} />
      </Route>
    </Route>
  )
);
