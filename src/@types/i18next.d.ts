import { resources } from '../core/i18n';

module 'i18next' {
  interface CustomTypeOptions {
    resources: (typeof resources)['en'];
  }
}
