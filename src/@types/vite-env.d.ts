/// <reference types="vite/client" />

interface ImportMetaEnv {
  readonly VITE_APP_ENV: 'Development' | 'Production';
}

interface ImportMeta {
  readonly env: ImportMetaEnv;
}
