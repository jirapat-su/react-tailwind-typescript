function AboutPage() {
  return <h1 className="text-3xl font-bold text-black dark:text-white">About</h1>;
}

export default AboutPage;
