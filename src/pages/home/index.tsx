import { useTranslation } from 'react-i18next';

function HomePage() {
  const { t } = useTranslation('homepage');

  return <h1 className="text-3xl font-bold text-black dark:text-white">{t('say')}</h1>;
}

export default HomePage;
