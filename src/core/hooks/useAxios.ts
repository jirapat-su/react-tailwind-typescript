import axios, { AxiosError, AxiosInstance, AxiosRequestConfig, AxiosResponse, CancelTokenSource } from 'axios';
import { useEffect, useState } from 'react';

interface UseAxiosResponse<T> {
  data: T | null;
  isLoading: boolean;
  isError: boolean;
  error: AxiosError<unknown> | null;
  fetch: () => void;
}

const REQUEST_TIMEOUT = 1000;
const httpClient: AxiosInstance = axios.create({
  timeout: REQUEST_TIMEOUT,
  headers: { 'X-Custom-Header': 'Custom-Header' }
});

export const useAxios = <T>(url: string, options: AxiosRequestConfig): UseAxiosResponse<T> => {
  const [data, setData] = useState<T | null>(null);
  const [isLoading, setIsLoading] = useState<boolean>(false);
  const [isError, setIsError] = useState<boolean>(false);
  const [error, setError] = useState<AxiosError<unknown> | null>(null);
  const [cancelTokenSource, setCancelTokenSource] = useState<CancelTokenSource | null>(null);

  useEffect(() => {
    return () => {
      if (cancelTokenSource) {
        cancelTokenSource.cancel('Request cancelled due to component unmount');
      }
    };
  }, [cancelTokenSource]);

  const fetchData = async () => {
    setIsLoading(true);
    setIsError(false);
    setError(null);

    const cancelToken = axios.CancelToken;
    const source = cancelToken.source();
    setCancelTokenSource(source);

    try {
      const response: AxiosResponse<T> = await httpClient(url, {
        ...options,
        cancelToken: source.token
      });
      setData(response.data);
    } catch (error) {
      if (!axios.isCancel(error)) {
        setIsError(true);
        setError(error as AxiosError<unknown>);
      }
    } finally {
      setIsLoading(false);
    }
  };

  const fetch = () => {
    fetchData();
  };

  return { data, isLoading, isError, error, fetch };
};
