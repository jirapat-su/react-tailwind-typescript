import i18n from 'i18next';
import { initReactI18next } from 'react-i18next';
import { en } from './en';
import { th } from './th';

const languageSupport = ['en', 'th'] as const;
const resources = { en, th } as const;

const generateFallbackLng = (() => {
  const languageObject: { [key: string]: string[] } = {};

  languageSupport.forEach((language, index) => {
    if (index === 0) {
      languageObject[language] = [language];
      languageObject.default = [language];
    } else {
      languageObject[language] = [language];
    }
  });

  return languageObject;
})();

i18n.use(initReactI18next).init({
  resources: resources,
  lng: languageSupport[0],
  fallbackLng: generateFallbackLng,
  ns: new Array(),
  defaultNS: false,
  supportedLngs: [...languageSupport],
  load: 'currentOnly',
  debug: import.meta.env.VITE_APP_ENV === 'Development',
  interpolation: {
    escapeValue: false
  },
  react: {
    useSuspense: false
  }
});

export default i18n;
export { languageSupport, resources };
