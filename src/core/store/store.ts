import { AnyAction, Dispatch, Middleware, configureStore } from '@reduxjs/toolkit';
import logger from 'redux-logger';
import { persistReducer, persistStore } from 'redux-persist';
import storage from 'redux-persist/lib/storage';
import thunk from 'redux-thunk';
import rootReducer from './rootReducer';

const persistConfig = {
  key: 'root',
  storage: storage,
  version: 1,
  whitelist: ['theme', 'language']
};

const appReducer = persistReducer(persistConfig, rootReducer);
const appMiddleware: Middleware<{}, any, Dispatch<AnyAction>>[] = [thunk];
if (import.meta.env.VITE_APP_ENV === 'Development') appMiddleware.push(logger);

export const store = configureStore({
  reducer: appReducer,
  middleware: appMiddleware
});

export const persistor = persistStore(store);
