import { combineReducers } from '@reduxjs/toolkit';
import { languageReducer } from './reducers/languageReducer';
import { themeReducer } from './reducers/themeReducer';

const rootReducer = combineReducers({
  theme: themeReducer,
  language: languageReducer
});

export default rootReducer;
export type RootState = ReturnType<typeof rootReducer>;
