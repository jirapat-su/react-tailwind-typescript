import { languageSupport } from '@/core/i18n';
import { createSlice, PayloadAction } from '@reduxjs/toolkit';

type LanguageProps = {
  value: (typeof languageSupport)[number];
};

const initialState: LanguageProps = {
  value: 'en'
};

const slice = createSlice({
  name: 'language',
  initialState,
  reducers: {
    setLanguage: (state, action: PayloadAction<LanguageProps['value']>) => {
      state.value = action.payload;
    }
  }
});

export const { setLanguage } = slice.actions;
export const languageReducer = slice.reducer;
