import { createSlice, PayloadAction } from '@reduxjs/toolkit';

type ThemeProps = {
  isDarkMode: boolean;
};

const initialState: ThemeProps = {
  isDarkMode: false
};

const slice = createSlice({
  name: 'theme',
  initialState,
  reducers: {
    setDarkMode: (state, action: PayloadAction<boolean>) => {
      state.isDarkMode = action.payload;
    }
  }
});

export const { setDarkMode } = slice.actions;
export const themeReducer = slice.reducer;
