import { languageSupport } from '@/core/i18n';
import { setLanguage } from '@/core/store/reducers/languageReducer';
import { setDarkMode } from '@/core/store/reducers/themeReducer';
import { RootState } from '@/core/store/rootReducer';
import { useCallback } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Link } from 'react-router-dom';

function NavBar() {
  const defaultLanguage = useSelector((state: RootState) => state.language.value);
  const isDarkMode = useSelector((state: RootState) => state.theme.isDarkMode);
  const dispatch = useDispatch();

  const handleChangeLanguage = useCallback(() => {
    const lang = languageSupport.find((lang) => lang != defaultLanguage);
    if (lang) dispatch(setLanguage(lang));
  }, [dispatch, defaultLanguage]);

  const handleChangeDarkMode = useCallback(() => {
    dispatch(setDarkMode(!isDarkMode));
  }, [dispatch, isDarkMode]);

  return (
    <div className="flex h-10 w-[100vw] items-center justify-between gap-4 bg-white px-4 text-black dark:bg-black dark:text-white">
      <div className="flex gap-4">
        <Link to="">Home</Link>
        <Link to="/about">About</Link>
      </div>
      <div className="flex gap-4">
        <a href="#" onClick={handleChangeLanguage}>
          {defaultLanguage === 'en' ? 'ไทย' : 'ENG'}
        </a>
        <a href="#" onClick={handleChangeDarkMode}>
          {isDarkMode ? 'Light' : 'Dark'}
        </a>
      </div>
    </div>
  );
}

export default NavBar;
