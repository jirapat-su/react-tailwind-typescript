import { Fragment, Suspense } from 'react';
import { Outlet } from 'react-router-dom';
import NavBar from './navbar';

function MainLayout() {
  return (
    <div>
      <NavBar />
      <div className="bg-white dark:bg-gray-900">
        <Suspense fallback={<Fragment>Loading...</Fragment>}>
          <Outlet />
        </Suspense>
      </div>
    </div>
  );
}

export default MainLayout;
