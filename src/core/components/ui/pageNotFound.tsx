function PageNotFound() {
  return (
    <div className="flex h-[calc(100vh-40px)] w-full flex-col items-center justify-center bg-white dark:bg-[rgba(0,0,0,0.4)]">
      <h1 className="text-9xl font-extrabold tracking-widest text-black dark:text-white">404</h1>
      <div className="rounded bg-[#FF6A3D] px-2 text-sm">Page Not Found</div>
    </div>
  );
}

export default PageNotFound;
