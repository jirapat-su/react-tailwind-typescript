import { Fragment, Suspense, useEffect } from 'react';
import { useTranslation } from 'react-i18next';
import { useSelector } from 'react-redux';
import { Outlet } from 'react-router-dom';
import { RootState } from './core/store/rootReducer';

export default function App() {
  const { i18n } = useTranslation();
  const theme = useSelector((state: RootState) => state.theme.isDarkMode);
  const language = useSelector((state: RootState) => state.language.value);

  useEffect(() => {
    i18n.changeLanguage(language);
  }, [language]);

  useEffect(() => {
    if (theme) document.documentElement.classList.add('dark');
    else document.documentElement.classList.remove('dark');
  }, [theme]);

  return (
    <Suspense fallback={<Fragment>Loading...</Fragment>}>
      <Outlet />
    </Suspense>
  );
}
