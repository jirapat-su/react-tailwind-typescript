/** @type {import('tailwindcss').Config} */
export default {
  darkMode: 'class',
  content: ['./index.html', './src/**/*.{jsx,tsx}'],
  theme: {
    extend: {}
  },
  plugins: []
};
